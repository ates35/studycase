﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tiklama : MonoBehaviour
{
    [HideInInspector]public GameObject sekerolusturmaobje;
    public int id;
    public List<GameObject> ButunObjelerimiAyirtma;
    public List<GameObject> EnYakinObjeler;
    public int Sira;

    // Start is called before the first frame update
    void Awake()
    {
        sekerolusturmaobje = GameObject.Find("olusturucu");

    }
    public void kontrol()
    {
        foreach (var TumObjeler in FindObjectsOfType(typeof(GameObject)) as GameObject[])
        {
            if (TumObjeler.name == "Banana Variant(Clone)" || TumObjeler.name == "Cherry Variant(Clone)" || TumObjeler.name == "Watermelon Variant(Clone)")
            {

                if (this.Sira != TumObjeler.GetComponent<tiklama>().Sira)
                {


                    ButunObjelerimiAyirtma.Add(TumObjeler);
                }
            }
        }
        for (int i = 0; i < ButunObjelerimiAyirtma.Count; i++)
        {
            float x = Mathf.Abs(this.transform.position.x - ButunObjelerimiAyirtma[i].transform.position.x);
            float z = Mathf.Abs(this.transform.position.z - ButunObjelerimiAyirtma[i].transform.position.z);
            if (x > 0 && this.id == ButunObjelerimiAyirtma[i].GetComponent<tiklama>().id)
            {
                if (z == 0)
                {
                    EnYakinObjeler.Add(ButunObjelerimiAyirtma[i]);
                }

            }
            if (z > 0 && this.id == ButunObjelerimiAyirtma[i].GetComponent<tiklama>().id)
            {
                if (x == 0)
                {
                    EnYakinObjeler.Add(ButunObjelerimiAyirtma[i]);
                }

            }

        }
        if (EnYakinObjeler.Count >= 2)
        {
            foreach (GameObject item in EnYakinObjeler)
            {
                Destroy(item);
            }
            Destroy(this.gameObject);
        }
        else
        {
            EnYakinObjeler.Clear();
            ButunObjelerimiAyirtma.Clear();
        }
    }
    private void OnMouseDown()
    {

        if (sekerolusturmaobje.GetComponent<mmm>().tiklanan1 == null)
        {
            sekerolusturmaobje.GetComponent<mmm>().tiklanan1 = this.gameObject;
        }
        else
        {
            sekerolusturmaobje.GetComponent<mmm>().tiklanan2 = this.gameObject;
            sekerolusturmaobje.GetComponent<mmm>().istenilen();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
