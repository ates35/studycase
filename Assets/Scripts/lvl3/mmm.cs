﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mmm : MonoBehaviour
{
    public int yukseklik;
    public int yataylik;
    public List<GameObject> Sekerlerimiz;
    [HideInInspector] public GameObject tiklanan1, tiklanan2;
    int sira;
    // Start is called before the first frame update
    void Start()
    {
        sekerolustur();
    }
    public void sekerolustur()
    {

        Instantiate(Sekerlerimiz[1], new Vector3(0, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(0, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(0, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(0, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(0, 0, 4), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(1, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(1, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(1, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(1, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(1, 0, 4), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(2, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(2, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(2, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(2, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(2, 0, 4), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(3, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(3, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(3, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(3, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(3, 0, 4), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(4, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(4, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(4, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(4, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(4, 0, 4), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(5, 0, 0), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(5, 0, 1), Quaternion.identity);
        Instantiate(Sekerlerimiz[2], new Vector3(5, 0, 2), Quaternion.identity);
        Instantiate(Sekerlerimiz[0], new Vector3(5, 0, 3), Quaternion.identity);
        Instantiate(Sekerlerimiz[1], new Vector3(5, 0, 4), Quaternion.identity);

    }
    public void istenilen()
    {
        float Gelen = Mathf.Abs(tiklanan1.transform.position.x - tiklanan2.transform.position.x);
        float Gelen1 = Mathf.Abs(tiklanan1.transform.position.z - tiklanan2.transform.position.z);

        if (Gelen <= 1 && Gelen1 <= 1)
        {
            Vector3 Tiklanan1 = tiklanan1.transform.position;
            Vector3 Tiklanan2 = tiklanan2.transform.position;
            tiklanan1.transform.position = Tiklanan2;
            tiklanan2.transform.position = Tiklanan1;
        }
        else
        {
            Debug.Log("uzak mesafe");
        }
        foreach (var TumObjeler in FindObjectsOfType(typeof(GameObject)) as GameObject[])
        {
            if (TumObjeler.name == "Bananalvl3 Variant(Clone)" || TumObjeler.name == "Cherrylvl3 Variant(Clone)" || TumObjeler.name == "Watermelonlvl3 Variant(Clone)")
            {
                TumObjeler.GetComponent<tiklama>().kontrol();
            }
        }
        tiklanan1 = null;
        tiklanan2 = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
