using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{

    public Transform banana;
    public Transform banana2;
    public float turnspeed;
    float towerangle;
    float cannonangle;
    // Start is called before the first frame update
   

    // Update is called once per frame
    void Update()
    {
        RotateTower();
        RotateCannon();

    }
    void RotateTower()
    {
        towerangle += Input.GetAxis("Mouse X") * turnspeed * -Time.deltaTime;
        towerangle = Mathf.Clamp(towerangle, 0, 180);
        banana.localRotation = Quaternion.AngleAxis(towerangle, Vector3.back);
    }
    void RotateCannon()
    {
        cannonangle += Input.GetAxis("Mouse Y") * turnspeed * -Time.deltaTime;
        cannonangle = Mathf.Clamp(cannonangle, -10, 10);
        banana.localRotation = Quaternion.AngleAxis(towerangle, Vector3.right);
    }

}
