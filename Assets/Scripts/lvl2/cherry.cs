﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cherry : MonoBehaviour
{
    public static cherry instance;
    [SerializeField]

    private GameObject banana;
    public int idcherry =1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

   private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "cherry")
        {

            if (idcherry > collision.gameObject.GetComponent<cherry>().idcherry)
            {
                BananaOlustur();
            }
            Destroy(collision.gameObject);
            Destroy(gameObject);


        }
    }

    void BananaOlustur()
    {
        GameObject yeni_banana = Instantiate(banana, transform.position, Quaternion.identity);
        banana.GetComponent<banana>().idbanana++;

    }
}
