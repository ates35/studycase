﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class banana : MonoBehaviour
{
    public static banana instance;
    [SerializeField]

    private GameObject hotdog;
    public int idbanana = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "banana")
        {


            if (idbanana > collision.gameObject.GetComponent<banana>().idbanana)
            {
                HotdogOlustur();
            }
            Destroy(collision.gameObject);
            Destroy(gameObject);


        }
    }

    void HotdogOlustur()
    {
        GameObject yeni_hotdog = Instantiate(hotdog, transform.position, Quaternion.identity);
        hotdog.GetComponent<hotdog>().idhotdog++;

    }
}
