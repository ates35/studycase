﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hotdog : MonoBehaviour
{
    public static hotdog instance;
    [SerializeField]

    private GameObject hamburger;
    public int idhotdog = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "hotdog")
        {


            if (idhotdog > collision.gameObject.GetComponent<hotdog>().idhotdog)
            {
                HamburgerOlustur();
            }
            Destroy(collision.gameObject);
            Destroy(gameObject);


        }
    }

    void HamburgerOlustur()
    {
        GameObject yeni_hamburger = Instantiate(hamburger, transform.position, Quaternion.identity);
        hamburger.GetComponent<hamburger>().idhamburger++;

    }
}
