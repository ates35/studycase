﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hamburger : MonoBehaviour
{
    public static hamburger instance;
    [SerializeField]

    private GameObject cheese;
    public int idhamburger = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "hamburger")
        {

            if (idhamburger > collision.gameObject.GetComponent<hamburger>().idhamburger)
            {
                CheeseOlustur();
            }
            Destroy(collision.gameObject);
            Destroy(gameObject);


        }
    }

    void CheeseOlustur()
    {
        GameObject yeni_cheese = Instantiate(cheese, transform.position, Quaternion.identity);
        cheese.GetComponent<cheese>().idcheese++;

    }
}
